# Status Store API
Requirements Summary
```

POST /statusupdates : should create a new resource /statusupdates/{id}
DELETE /statusupdates/{id} should delete that resource
GET /statusupdates : gets all the resources
GET /statusupdates/{id} : should get that specific resource

#Deployement Requirements

Need to upload the labada code to s3Bucket as zip file and specify the s3 name and file name of the code as parameter.

Deployment code
aws cloudformation deploy --template-file cf.yml --stack-name {Stack name} --capabilities CAPABILITY_IAM --parameter-overrides DBName={Any name} s3Name={Bucket name} fileName={Zip file name}

# Sample data for post
{
   "id":"1",
   "year": "2013",
   "title": "Prisoners",
   "directors": "Denis Villeneuve",
   "release_date": "2013-08-30T00:00:00",
   "rating": "8.2"
}