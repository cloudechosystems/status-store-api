var AWS = require('aws-sdk');
var dynamodb = new AWS.DynamoDB();
//Get Param Data For An Id... 

function getSingle(objectId, callback, DB_Table) {
  console.log("Querying from table");
  var response;
  var docClient = new AWS.DynamoDB.DocumentClient();
  var params = {
    TableName: DB_Table,
    KeyConditionExpression: "#id = :objectid",
    ExpressionAttributeNames: {
      "#id": "id"
    },
    ExpressionAttributeValues: {
      ":objectid": objectId
    }
  };

  docClient.query(params, function(err, data) {
    if (err) {
      console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
    } else {
      console.log("Query succeeded.");
      data.Items.forEach(function(item) {
        console.log(JSON.stringify(data.Items));
      });
    }
    response = {
      "statusCode": 200,
      "headers": {
        "my_header": "my_value"
      },
      "body": JSON.stringify(data),
      "isBase64Encoded": false
    };
    callback(null, response);
  });


}

//Get All...

function getList(context, callback, DB_Table) {
  console.log("Querying from table");
  var response;
  var docClient = new AWS.DynamoDB.DocumentClient();
  var params = {
    TableName: DB_Table
  };

  docClient.scan(params, function(err, data) {
    if (err) {
      console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
    } else {
      console.log("Query succeeded.");
      data.Items.forEach(function(item) {
        console.log(JSON.stringify(data.Items));
      });
    }
    response = {
      "statusCode": 200,
      "headers": {
        "my_header": "my_value"
      },
      "body": JSON.stringify(data),
      "isBase64Encoded": false
    };
    callback(null, response);
  });
}


//Post Item...

function putItem(event, callback, DB_Table) {
  let body = JSON.parse(event.body);
  var params = {
    Item: {
      "id": {
        S: body['id']
      },
      "year": {
        S: body['year']
      },
      "title": {
        S: body['title']
      },
      "directors": {
        S: body['directors']
      },
      "release_date": {
        S: body['release_date']
      },
      "rating": {
        S: body['rating']
      }
    },
    ReturnConsumedCapacity: "TOTAL",
    TableName: DB_Table
  };
  dynamodb.putItem(params, function(err, data) {
    if (err) console.log(err, err.stack); // an error occurred
    else console.log(data); // successful response
  });
  var response = {
    "statusCode": 200,
    "headers": {
      "my_header": "my_value"
    },
    "body": "Data Inserted",
    "isBase64Encoded": false
  };
  callback(null, response);
}

//Delete item...

function putDelete(id, callback, DB_Table) {
  var docClient = new AWS.DynamoDB.DocumentClient();
  var params = {
    TableName: DB_Table,
    Key: {
      "id": id
    }
  };

  console.log("Attempting a conditional delete...");
  docClient.delete(params, function(err, data) {
    if (err) {
      console.error("Unable to delete item. Error JSON:", JSON.stringify(err, null, 2));
      var response = {
        "statusCode": 200,
        "headers": {
          "my_header": "my_value"
        },
        "body": "Row Deleted Failed",
        "isBase64Encoded": false
      };
      callback(null, response);
    } else {
      console.log("DeleteItem succeeded:", JSON.stringify(data, null, 2));
      var response = {
        "statusCode": 200,
        "headers": {
          "my_header": "my_value"
        },
        "body": "Row Deleted",
        "isBase64Encoded": false
      };
      callback(null, response);
    }
  });

}

exports.myHandler = (event, context, callback) => {
  var DB_Table = process.env.DB_Table;
  switch (event.httpMethod) {
    case 'GET':
      {
        if (event.pathParameters) {
          getSingle(event.pathParameters.id, callback, DB_Table);
        } else {
          getList(context, callback, DB_Table);
        }
        break;
      }

    case 'POST':
      {
        putItem(event, callback, DB_Table);
        break;
      }
    case 'DELETE':
      {
        putDelete(event.pathParameters.id, callback, DB_Table);
        break;
      }
  }
};